package com.example.chhayrith.linkdatabase

class Student{
    var id: Int = 0
    var name: String = ""
    var roll: Int = 0

    constructor(id: Int, name: String, roll: Int){
        this.id = id
        this.roll = roll
        this.name = name
    }

    constructor()
}