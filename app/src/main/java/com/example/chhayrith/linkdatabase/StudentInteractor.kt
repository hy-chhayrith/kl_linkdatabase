package com.example.chhayrith.linkdatabase

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

// Global variable
val DB_NAME = "B4";
val TB_NAME = "STUDENT"
val COL_ID = "id";
val COL_NAME = "name"
val COL_ROLL = "roll"

class StudentInteractor(context: Context): SQLiteOpenHelper(context, DB_NAME, null, 1) {
    override fun onCreate(db: SQLiteDatabase?) {
        var createTable = "create table $DB_NAME ( $COL_ID integer primary key autoincrement, $COL_NAME varchar(256), $COL_ROLL integer)"

        // execute query
        db?.execSQL(createTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    // interact with database
    fun insertData(s: Student){
        var db = this.writableDatabase
        var content_value = ContentValues()

        // put value through content value
        content_value.put(COL_NAME, s.name)
        content_value.put(COL_ROLL, s.id)

        // insert data to database in a writable class
        db.insert(TB_NAME, null, content_value)
    }
    fun getData():ArrayList<Student>{
        var db = this.readableDatabase
        var query = "select * from $TB_NAME"
        var student_list: ArrayList<Student>

        // query database
        var result = db.rawQuery(query, null)

    }
}